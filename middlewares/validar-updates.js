const { getUsuarioByEmail, getUsuarioByIdColeccion } = require("../helpers/get-usuario");
const { Cliente } = require("../models");

const validarUpdateEmailAdmin = async (req, res, next) => {
    try {
        if (!req.rolAuth || !req.usuarioAuth) {
            return res.status(400).json({
                msg: 'Ruta inválida para la acción solicitada'
            });
        }

        const { email } = req.body;
        const { id } = req.params;
        const adminDBNuevo = await getUsuarioByEmail('admin', email);

        if (adminDBNuevo) {
            const adminDBId = await getUsuarioByIdColeccion('admin', id);
            if (adminDBId.email !== adminDBNuevo.email) {
                return res.status(400).json({
                    msg: 'El email ingresado ya existe para otro usuario'
                });
            }
        }

        next();

    } catch (error) {
        res.status(500).json({
            msg: 'Error validando el email del admin'
        })
    }
}

const validarUpdateEmailAgente = async (req, res, next) => {
    try {
        if (!req.rolAuth || !req.usuarioAuth) {
            return res.status(400).json({
                msg: 'Ruta inválida para la acción solicitada'
            });
        }

        const { email } = req.body;
        const { id } = req.params;
        const agenteDBNuevo = await getUsuarioByEmail('agente', email);

        if (agenteDBNuevo) {
            const agenteDBId = await getUsuarioByIdColeccion('agente', id);
            if (agenteDBId.email !== agenteDBNuevo.email) {
                return res.status(400).json({
                    msg: 'El email ingresado ya existe para otro usuario'
                });
            }
        }

        next();

    } catch (error) {
        res.status(500).json({
            msg: 'Error validando el email del agente'
        })
    }
}


const validarUpdateEmailCliente = async (req, res, next) => {
    try {
        if (!req.rolAuth || !req.usuarioAuth) {
            return res.status(400).json({
                msg: 'Ruta inválida para la acción solicitada'
            });
        }

        const { email } = req.body;
        const { id } = req.params;
        const clienteDBNuevo = await Cliente.findOne({ email });

        if (clienteDBNuevo) {
            const clienteDBId = await Cliente.findById(id);
            if (clienteDBId.email !== clienteDBNuevo.email) {
                return res.status(400).json({
                    msg: 'El email ingresado ya existe para otro usuario'
                });
            }
        }

        next();

    } catch (error) {
        res.status(500).json({
            msg: 'Error validando el email del cliente'
        })
    }
}



module.exports = {
    validarUpdateEmailAdmin, validarUpdateEmailAgente, validarUpdateEmailCliente
}