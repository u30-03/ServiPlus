const valCampos = require('./validar-campos');
const valJWT = require('./validar-jwt');
const valRol = require('./validar-rol');
const getModelo = require('./get-modelo');
const agregarTipo = require('./agregar-tipo');
const valUpdates = require('./validar-updates')

module.exports = {
    ...valCampos,
    ...valJWT,
    ...valRol,
    ...getModelo,
    ...agregarTipo,
    ...valUpdates
}