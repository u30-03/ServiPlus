const { response } = require('express');
const { getUsuarioById } = require('../helpers/get-usuario');

const agregarAdmin = async (req, res = response, next) => {
    req.tipo = 'admin';
    next();
}

const agregarAgente = async (req, res = response, next) => {
    req.tipo = 'agente';
    next();
}

/* const agregarTipo = async(req, res=response, next) => {
    const user = getUsuarioById(req.)
    next();
} */


module.exports = {
    agregarAdmin,
    agregarAgente
}