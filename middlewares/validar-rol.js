const { response } = require('express');

const validarRol = (...roles) => {
    return (req, res = response, next) => {

        // if usuarioAuth is not part of req, e.g. if validationRole comes first that validateJWT
        if (!req.rolAuth) {
            return res.status(400).json({
                msg: 'Ruta inválida para la acción solicitada'
            });
        }

        if (!roles.includes(req.rolAuth)) {
            return res.status(401).json({
                msg: 'Acción no permitida'
            });
        }
        next();
    }
}

module.exports = {
    validarRol
}