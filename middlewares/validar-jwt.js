const { response } = require('express');
const jwt = require('jsonwebtoken');
const { getUsuarioById } = require('../helpers/get-usuario');

const validarJWT = async (req, res = response, next) => {
    const token = req.header('auth-token');

    if (!token) {
        return res.status(401).json({
            msg: 'Información de autenticación inválida'
        });
    }

    try {
        // extraer uid del payload
        const { uid: authUid } = jwt.verify(token, process.env.SECRET_JWT_KEY);

        // Obtener el usuario que realiza la petición        
        const { rol: rolAuth, modelo: usuarioAuth } = await getUsuarioById(authUid);
        // Prevenir que ingrese si el usuario no existe
        if (!usuarioAuth) {
            return res.status(401).json({
                msg: 'Información de autenticación inválida1'
            })
        }

        // Verificar si el estado del usuario es activo
        if (!usuarioAuth.estado) {
            return res.status(401).json({
                msg: 'Información de autenticación inválida2'
            });
        }

        // añadir el usuario a la petición
        req.usuarioAuth = usuarioAuth.toJSON();
        req.rolAuth = rolAuth;
        next();

    } catch (error) {
        console.log(error);
        res.status(401).json({
            msg: 'Información de autenticación inválida3'
        });
    }

}

module.exports = {
    validarJWT
}