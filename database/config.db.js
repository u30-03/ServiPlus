const mongoose = require('mongoose');

const dbConnection = async () => {
    try {
        mongoose.connect(process.env.MONGODB_CNN);
        console.log('Conectado a la base de datos');

        /* mongoose.connect(process.env.MONGODB_CNN, {
            useNewUrlParser: true,
            useCreateIndex: true,
            useUnifiedTopology: true,
            useFindAndModify: false
        }); */

    } catch (err) {
        console.log(err);
        throw new Error('Error tratando de acceder a la base de datos');
    }
}

module.exports = {
    dbConnection
}