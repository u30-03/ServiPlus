const { response } = require('express');
const bcryptjs = require('bcryptjs');
const { Admin } = require('../models');

const createAdmin = async (req, res = response) => {
    try {
        const { nombreCompleto, email, telefono, password } = req.body;
        /* const adminDB = await Admin.findOne({ email });
        if (adminDB) {
            return res.status(400).json({
                msg: `El Admin con email '${email}' ya existe`
            });
        } */

        // chequear si es un admin que está inactivo, en cuyo caso se reactiva con los nuevos datos
        const adminDB = await Admin.findOne({ email: email });

        if (adminDB) {
            if (!adminDB.estado) {
                req.body.estado = true;
                req.params.id = adminDB._id;
                await updateAdmin(req, res)
                return
            }
        }

        // el usuario y el estado pueden ser provistos desde el front
        // por lo tanto, es mejor asegurarse que tienen valores por defecto
        const dataAdmin = {
            nombreCompleto,
            email,
            telefono
        }

        const salt = bcryptjs.genSaltSync();
        dataAdmin.password = bcryptjs.hashSync(password.trim(), salt);

        const admin = new Admin(dataAdmin);
        await admin.save();

        res.status(201).json({
            msg: 'Admin creado',
            admin
        });

    } catch (error) {
        console.log(error);
        return res.status(500).json({
            msg: 'Error registrando el nuevo admin'
        });
    }
}


const getAdmin = async (req, res = response) => {
    const { id } = req.params;
    const adminDB = await Admin.findById(id);
    res.json({
        admin: adminDB
    })
}

const listAdmin = async (req, res = response) => {
    const { limit = 5, from = 0 } = req.query;
    const qry = { estado: true };

    const [total, admins] = await Promise.all([
        Admin.countDocuments(qry),
        Admin.find(qry)
            .skip(Number(from))
            .limit(Number(limit))
    ])

    res.json({
        total, admins
    });
}


const updateAdmin = async (req, res = response) => {

    try {
        const { id } = req.params;
        const { _id, ...theAdmin } = req.body; //prevenir sobreescritura de _id

        // controlar estados truthy y falsy, estado no es validado
        if (theAdmin.estado !== null && theAdmin.estado !== undefined) {
            theAdmin.estado = theAdmin.estado ? true : false;
        }

        if (theAdmin.password) {
            const salt = bcryptjs.genSaltSync();
            theAdmin.password = bcryptjs.hashSync(theAdmin.password.trim(), salt);
        }

        const adminDB = await Admin.findByIdAndUpdate(id, theAdmin, { new: true });

        res.json({
            msg: 'admin actualizado',
            adminDB
        });

    } catch (error) {
        console.log(error);
        return res.status(500).json({
            msg: 'Error actualizando el admin'
        });
    }

}

const deleteAdmin = async (req, res = response) => {
    const { id } = req.params;
    const adminDB = await Admin.findByIdAndUpdate(id, { estado: false }, { new: true });
    res.status(202).json({
        adminDB
    });
}

module.exports = {
    createAdmin,
    getAdmin,
    listAdmin,
    updateAdmin,
    deleteAdmin
}