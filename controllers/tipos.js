const { response } = require('express');
const { Tipo } = require('../models');

const createTipo = async (req, res = response) => {
    try {
        const { descripcionTipo, tiempo } = req.body;

        // el usuario y el estado pueden ser provistos desde el front
        // por lo tanto, es mejor asegurarse que tienen valores por defecto
        const dataTipo = {
            descripcionTipo: descripcionTipo.toUpperCase(),
            tiempo
        }

        const tipo = new Tipo(dataTipo);
        await tipo.save();

        res.status(201).json({
            msg: 'Tipo creado',
            tipo
        });

    } catch (error) {
        console.log(error);
        return res.status(500).json({
            msg: 'Error registrando el nuevo tipo'
        });
    }
}


const getTipo = async (req, res = response) => {
    const { id } = req.params;
    const tipoDB = await Tipo.findById(id);
    res.json({
        tipo: tipoDB
    })
}

const listTipo = async (req, res = response) => {
    const { limit = 5, from = 0 } = req.query;
    const qry = { estado: true };

    const [total, tipos] = await Promise.all([
        Tipo.countDocuments(qry),
        Tipo.find(qry)
            .skip(Number(from))
            .limit(Number(limit))
    ]);

    res.json({
        total, tipos
    });
}


const updateTipo = async (req, res = response) => {

    try {
        const { id } = req.params;
        const { _id, ...theTipo } = req.body; //prevenir sobreescritura de _id

        //si el nombre del tipo existe pero es él mismo, se deja actualizar
        theTipo.descripcionTipo = theTipo.descripcionTipo.toUpperCase();
        const tipoCheck = await Tipo.findOne({ descripcionTipo: theTipo.descripcionTipo });
        if (tipoCheck && tipoCheck.id !== id) {
            return res.status(400).json({
                msg: 'Ya existe otro tipo con el mismo nombre'
            });
        }

        // controlar estados truthy y falsy, estado no es validado
        if (theTipo.estado !== null && theTipo.state !== undefined) {
            theTipo.estado = theTipo.estado ? true : false;
        }

        const tipoDB = await Tipo.findByIdAndUpdate(id, theTipo, { new: true });

        res.json({
            msg: 'Tipo actualizado',
            tipoDB
        });

    } catch (error) {
        console.log(error);
        return res.status(500).json({
            msg: 'Error actualizando el tipo'
        });
    }

}

const deleteTipo = async (req, res = response) => {
    const { id } = req.params;
    const tipoDB = await Tipo.findByIdAndUpdate(id, { estado: false }, { new: true });
    res.status(202).json({
        tipoDB
    });
}

module.exports = {
    createTipo,
    getTipo,
    listTipo,
    updateTipo,
    deleteTipo
}