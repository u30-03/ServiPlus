const { response } = require('express');
const { Historia, Ticket, Agente, Admin } = require('../models');

const createHistoria = async (req, res = response) => {
    try {

        if (!req.rolAuth || !req.usuarioAuth) {
            return res.status(400).json({
                msg: 'Ruta inválida para la acción solicitada'
            });
        }

        const { idTicket, observacion = '', agenteAsigna: idAgenteAsigna, evento } = req.body;

        const dataHistoria = {
            observacion, evento
        }

        if (idAgenteAsigna !== '') {
            const agenteDB = await Agente.findById(idAgenteAsigna);
            dataHistoria.agenteAsigna = agenteDB;
            dataHistoria.areaAsigna = agenteDB.area;
        }

        const ticketDB = await Ticket.findById(idTicket);
        dataHistoria.ticket = ticketDB;

        dataHistoria.estadoProceso = ticketDB.estadoProceso;
        dataHistoria.tipoTicket = ticketDB.tipoTicket;

        dataHistoria.fechaMod = Date.now();


        switch (req.rolAuth) {
            case 'admin':
                dataHistoria.adminMod = await Admin.findById(req.usuarioAuth.uid);
                break;
            case 'agente':
                dataHistoria.agenteMod = await Agente.findById(req.usuarioAuth.uid);
                break;
            default:
                return res.status(400).json({
                    msg: 'Ruta inválida para la acción solicitada, no hay un rol para la historia'
                });
        }

        const historia = new Historia(dataHistoria);
        let historiaNew = await historia.save();

        const historiaDB = await Historia.findById(historiaNew.id)
            .populate('estadoProceso', 'nombreEstado')
            .populate('tipoTicket', 'descripcionTipo')
            .populate('agenteAsigna', ['email'])
            .populate('areaAsigna', 'nombreArea')
            .populate('agenteMod', 'email')
            .populate('adminMod', 'email');

        //const historiaNew = await Historia.findOne({ email: historia.email }).populate('area', 'nombreArea');

        res.status(201).json({
            msg: 'Historia creada',
            historia: historiaDB
        });

    } catch (error) {
        console.log(error);
        return res.status(500).json({
            msg: 'Error registrando la nueva historia'
        });
    }
}


const getHistoria = async (req, res = response) => {
    const { id } = req.params;
    const historiaDB = await Historia.findById(id)
        .populate('estadoProceso', 'nombreEstado')
        .populate('tipoTicket', 'descripcionTipo')
        .populate('agenteAsigna', 'email')
        .populate('areaAsigna', 'nombreArea')
        .populate('agenteMod', 'email')
        .populate('adminMod', 'email');
    res.json({
        historia: historiaDB
    })
}

const listHistoria = async (req, res = response) => {
    const { limit = 5, from = 0 } = req.query;
    const qry = { estado: true };

    const [total, historias] = await Promise.all([
        Historia.countDocuments(qry),
        Historia.find(qry)
            .skip(Number(from))
            .limit(Number(limit))
            .populate('estadoProceso', 'nombreEstado')
            .populate('tipoTicket', 'descripcionTipo')
            .populate('agenteAsigna', 'email')
            .populate('areaAsigna', 'nombreArea')
            .populate('agenteMod', 'email')
            .populate('adminMod', 'email')
    ])

    res.json({
        total, historias
    });
}


const deleteHistoria = async (req, res = response) => {
    /* const { id } = req.params;
    const historiaDB = await Historia.findByIdAndUpdate(id, { estado: false }, { new: true });
    res.status(202).json({
        historiaDB
    }); */
}


const updateHistoria = async (req, res = response) => {
    /* 
        try {
            const { id } = req.params;
            const { _id, ...theHistoria } = req.body; //prevenir sobreescritura de _id
    
            // controlar estados truthy y falsy, estado no es validado
            if (theHistoria.estado !== null && theHistoria.estado !== undefined) {
                theHistoria.estado = theHistoria.estado ? true : false;
            }
    
            if (theHistoria.cliente) {
                const clienteDB = await Cliente.findOne({ email: theHistoria.cliente.toLowerCase() });
                theHistoria.cliente = clienteDB;
            }
    
            if (theHistoria.tipoHistoria) {
                const tipoDB = await Tipo.findOne({ descripcionTipo: theHistoria.tipoHistoria.toUpperCase() });
                theHistoria.tipoHistoria = tipoDB;
            }
    
            if (theHistoria.estadoProceso) {
                const estadoDB = await Estado.findOne({ nombreEstado: theHistoria.estadoProceso.toUpperCase() });
                theHistoria.estadoProceso = estadoDB;
            }
    
            const historiaDB = await Historia.findByIdAndUpdate(id, theHistoria, { new: true });
    
            res.json({
                msg: 'historia actualizado',
                historiaDB
            });
    
        } catch (error) {
            console.log(error);
            return res.status(500).json({
                msg: 'Error actualizando el historia'
            });
        } */

}

module.exports = {
    createHistoria,
    getHistoria,
    listHistoria,
    deleteHistoria,
    updateHistoria
} 