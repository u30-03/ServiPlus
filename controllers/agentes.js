const { response } = require('express');
const bcryptjs = require('bcryptjs');
const { Agente, Area } = require('../models');

const createAgente = async (req, res = response) => {
    try {
        const { nombreCompleto, email, telefono, password, area } = req.body;

        // chequear si es un agente que está inactivo, en cuyo caso se reactiva con los nuevos datos
        const agenteDB = await Agente.findOne({ email: email });

        if (agenteDB) {
            if (!agenteDB.estado) {
                req.body.estado = true;
                req.params.id = agenteDB._id;
                await updateAgente(req, res)
                return
            }
        }

        // el usuario y el estado pueden ser provistos desde el front
        // por lo tanto, es mejor asegurarse que tienen valores por defecto
        const dataAgente = {
            nombreCompleto,
            email,
            telefono
        }

        const salt = bcryptjs.genSaltSync();
        dataAgente.password = bcryptjs.hashSync(password.trim(), salt);

        const areaDB = await Area.findOne({ nombreArea: area.toUpperCase() });
        dataAgente.area = areaDB;

        const agente = new Agente(dataAgente);
        await agente.save();

        const agenteNew = await Agente.findOne({ email: agente.email }).populate('area', 'nombreArea');

        res.status(201).json({
            msg: 'Agente creado',
            agente: agenteNew
        });

    } catch (error) {
        console.log(error);
        return res.status(500).json({
            msg: 'Error registrando el nuevo agente'
        });
    }
}

const getAgente = async (req, res = response) => {
    const { id } = req.params;
    const agenteDB = await Agente.findById(id).populate('area', 'nombreArea');
    res.json({
        agente: agenteDB
    })
}

const listAgente = async (req, res = response) => {
    const { limit = 5, from = 0 } = req.query;
    const qry = { estado: true };

    const [total, agentes] = await Promise.all([
        Agente.countDocuments(qry),
        Agente.find(qry)
            .skip(Number(from))
            .limit(Number(limit))
            .populate('area', 'nombreArea')
    ])

    res.json({
        total, agentes
    });
}



const deleteAgente = async (req, res = response) => {
    const { id } = req.params;
    const agenteDB = await Agente.findByIdAndUpdate(id, { estado: false }, { new: true });
    res.status(202).json({
        agenteDB
    });
}


const updateAgente = async (req, res = response) => {

    try {
        const { id } = req.params;
        const { _id, ...theAgente } = req.body; //prevenir sobreescritura de _id

        // controlar estados truthy y falsy, estado no es validado
        if (theAgente.estado !== null && theAgente.estado !== undefined) {
            theAgente.estado = theAgente.estado ? true : false;
        }

        if (theAgente.password) {
            const salt = bcryptjs.genSaltSync();
            theAgente.password = bcryptjs.hashSync(theAgente.password.trim(), salt);
        }

        if (theAgente.area) {
            const areaDB = await Area.findOne({ nombreArea: theAgente.area.toUpperCase() });
            theAgente.area = areaDB;
        }

        const agenteDB = await Agente.findByIdAndUpdate(id, theAgente, { new: true });

        res.json({
            msg: 'agente actualizado',
            agenteDB
        });

    } catch (error) {
        console.log(error);
        return res.status(500).json({
            msg: 'Error actualizando el agente'
        });
    }

}

module.exports = {
    createAgente,
    getAgente,
    listAgente,
    deleteAgente,
    updateAgente
} 