const { response } = require('express');
const { getUltimaHistoria } = require('../helpers/get-historia');
const { Ticket, Cliente, Tipo, Estado } = require('../models');

const createTicket = async (req, res = response) => {
    try {
        const { cliente, tipoTicket, fechaApertura, fechaCierre, descripcion, estadoProceso, solucion } = req.body;

        if (!fechaApertura) {
            fechaApertura = new Date();
        }

        const dataTicket = {
            fechaApertura, fechaCierre, descripcion, solucion
        }

        const clienteDB = await Cliente.findOne({ email: cliente.toLowerCase() });
        dataTicket.cliente = clienteDB;

        const tipoDB = await Tipo.findOne({ descripcionTipo: tipoTicket.toUpperCase() });
        dataTicket.tipoTicket = tipoDB;

        const estadoDB = await Estado.findOne({ nombreEstado: estadoProceso.toUpperCase() });
        dataTicket.estadoProceso = estadoDB;

        const ticket = new Ticket(dataTicket);
        let ticketNew = await ticket.save();

        const ticketDB = await Ticket.findById(ticketNew.id)
            .populate('estadoProceso', 'nombreEstado')
            .populate('cliente', ['nombreCompleto', 'email'])
            .populate('tipoTicket', 'descripcionTipo');

        //const ticketNew = await Ticket.findOne({ email: ticket.email }).populate('area', 'nombreArea');

        res.status(201).json({
            msg: 'Ticket creado',
            ticket: ticketDB
        });

    } catch (error) {
        console.log(error);
        return res.status(500).json({
            msg: 'Error registrando el nuevo ticket'
        });
    }
}


const getTicket = async (req, res = response) => {
    const { id } = req.params;
    const ticketDB = await Ticket.findById(id)
        .populate('estadoProceso', 'nombreEstado')
        .populate('cliente', ['nombreCompleto', 'email'])
        .populate('tipoTicket', 'descripcionTipo');
    res.json({
        ticket: ticketDB
    })
}


const getUltHistoriaTicket = async (req, res = response) => {
    const { id } = req.params;
    const ticketDB = await getUltimaHistoria(id);
    if (ticketDB.length > 0) {
        return res.status(200).json(
            ticketDB[0]
        );
    }
    return res.status(200).json({
        ticket: []
    });
}

const listTicket = async (req, res = response) => {
    const { limit = 5, from = 0 } = req.query;
    const qry = { estado: true };

    const [total, tickets] = await Promise.all([
        Ticket.countDocuments(qry),
        Ticket.find(qry)
            .skip(Number(from))
            .limit(Number(limit))
            .populate('estadoProceso', 'nombreEstado')
            .populate('cliente', ['nombreCompleto', 'email'])
            .populate('tipoTicket', 'descripcionTipo')
    ])

    res.json({
        total, tickets
    });
}



const deleteTicket = async (req, res = response) => {
    const { id } = req.params;
    const ticketDB = await Ticket.findByIdAndUpdate(id, { estado: false }, { new: true });
    res.status(202).json({
        ticketDB
    });
}


const updateTicket = async (req, res = response) => {

    try {
        const { id } = req.params;
        const { _id, ...theTicket } = req.body; //prevenir sobreescritura de _id

        // verificar solución cuando el estado es solucionado
        if (theTicket.estadoProceso?.toUpperCase() == 'SOLUCIONADO') {
            if (theTicket.solucion?.length < 10) {
                return res.status(404).json({
                    msg: 'La descripción de la solución es obligatoria'
                })
            }
        }

        // verificar que se manda fecha de cierre o cancelación
        if (theTicket.estadoProceso?.toUpperCase() == 'SOLUCIONADO' || theTicket.estadoProceso?.toUpperCase() == 'CANCELADO') {
            if (!theTicket.fechaCierre) {
                return res.status(404).json({
                    msg: 'La fecha de cierre es obligatoria si se cambia a solucionado / cancelado'
                })
            }
        }

        // controlar estados truthy y falsy, estado no es validado
        if (theTicket.estado !== null && theTicket.estado !== undefined) {
            theTicket.estado = theTicket.estado ? true : false;
        }

        if (theTicket.cliente) {
            const clienteDB = await Cliente.findOne({ email: theTicket.cliente.toLowerCase() });
            theTicket.cliente = clienteDB;
        }

        if (theTicket.tipoTicket) {
            const tipoDB = await Tipo.findOne({ descripcionTipo: theTicket.tipoTicket.toUpperCase() });
            theTicket.tipoTicket = tipoDB;
        }

        if (theTicket.estadoProceso) {
            const estadoDB = await Estado.findOne({ nombreEstado: theTicket.estadoProceso.toUpperCase() });
            theTicket.estadoProceso = estadoDB;
        }

        if (theTicket.fechaCierre) {
            theTicket.fechaCierre = new Date(theTicket.fechaCierre);
        }

        /* if (theTicket.fechaApertura) {
            theTicket.fechaApertura = new Date(theTicket.fechaApertura);
        } */

        const ticketDB = await Ticket.findByIdAndUpdate(id, theTicket, { new: true });

        res.json({
            msg: 'ticket actualizado',
            ticketDB
        });

    } catch (error) {
        console.log(error);
        return res.status(500).json({
            msg: 'Error actualizando el ticket'
        });
    }

}

module.exports = {
    createTicket,
    getTicket,
    listTicket,
    deleteTicket,
    updateTicket,
    getUltHistoriaTicket
} 