const { response } = require('express');
const { Area } = require('../models');

const createArea = async (req, res = response) => {
    try {
        const { nombreArea } = req.body;

        // el usuario y el estado pueden ser provistos desde el front
        // por lo tanto, es mejor asegurarse que tienen valores por defecto
        const dataArea = {
            nombreArea: nombreArea.toUpperCase()
        }

        const areaCheck = await Area.findOne({ nombreArea: dataArea.nombreArea });

        if (areaCheck) {
            // controlar estados truthy y falsy, estado no es validado
            if (!areaCheck.estado) {
                areaCheck.estado = true;
                const areaDB = await Area.findByIdAndUpdate(areaCheck.id, areaCheck, { new: true });
                return res.status(201).json({
                    msg: 'Área actualizada (estado)',
                    areaDB
                });
            } else {
                return res.status(400).json({
                    errors: [
                        { msg: 'El área ingresada ya existe' }
                    ]
                })
            }
        } else {
            const area = new Area(dataArea);
            await area.save();

            return res.status(201).json({
                msg: 'Área creada',
                area
            });
        }
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            msg: 'Error registrando la nueva área'
        });
    }
}


const getArea = async (req, res = response) => {
    const { id } = req.params;
    const areaDB = await Area.findById(id);
    res.json({
        area: areaDB
    })
}

const listArea = async (req, res = response) => {
    const { limit = 5, from = 0 } = req.query;
    const qry = { estado: true };

    const [total, areas] = await Promise.all([
        Area.countDocuments(qry),
        Area.find(qry)
            .skip(Number(from))
            .limit(Number(limit))
    ])

    res.json({
        total, areas
    });
}


const updateArea = async (req, res = response) => {

    try {
        const { id } = req.params;
        const { _id, ...theArea } = req.body; //prevenir sobreescritura de _id

        //si el nombre del área existe pero es ella misma, se deja actualizar
        theArea.nombreArea = theArea.nombreArea.toUpperCase();
        const areaCheck = await Area.findOne({ nombreArea: theArea.nombreArea });
        if (areaCheck && areaCheck.id !== id) {
            return res.status(400).json({
                msg: 'Ya existe otra área con el mismo nombre'
            });
        }

        // controlar estados truthy y falsy, estado no es validado
        if (theArea.estado !== null && theArea.state !== undefined) {
            theArea.estado = theArea.estado ? true : false;
        }

        const areaDB = await Area.findByIdAndUpdate(id, theArea, { new: true });

        res.json({
            msg: 'Área actualizada',
            areaDB
        });

    } catch (error) {
        console.log(error);
        return res.status(500).json({
            msg: 'Error actualizando el área'
        });
    }

}

const deleteArea = async (req, res = response) => {
    const { id } = req.params;
    const areaDB = await Area.findByIdAndUpdate(id, { estado: false }, { new: true });
    res.status(202).json({
        areaDB
    });
}

module.exports = {
    createArea,
    getArea,
    listArea,
    updateArea,
    deleteArea
}