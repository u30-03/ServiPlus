const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');

const { response } = require('express');
const { generarJWT } = require('../helpers/generar-jwt');
const { getUsuarioByEmail } = require('../helpers/get-usuario');
const { getUsuarioById } = require('../helpers/get-usuario');

const login = async (req, res = response) => {

    const { email, password } = req.body;
    const tipo = req.tipo;

    try {

        // verificar si el email existe
        const user = await getUsuarioByEmail(tipo, email);
        if (!user) {
            return res.status(400).json({
                msg: 'La información de usuario / contraseña no es válida!' // (email)
            });
        }

        // verificar si está activo
        if (!user.estado) {
            return res.status(400).json({
                msg: 'Ha ocurrido un error. Comuníquese con el administrador.' //  (state: false)
            });
        }

        // validar password
        const validPassword = bcryptjs.compareSync(password, user.password);
        if (!validPassword) {
            return res.status(400).json({
                msg: 'La información de usuario / contraseña no es válida!' //  (password)
            });
        }

        // Generar JSONWebToken
        const token = await generarJWT(user.id);

        res.json({
            user,
            token
        });

    } catch (error) {
        console.log(error);
        return res.status(500).json({
            msg: 'Ocurrió un error mientras se trataba de iniciar sesión!'
        });
    }

}

const isValidJWT = async (req, res = response) => {
    /* console.log(req);
    console.log(req.header('auth-token')) */
    const token = req.header('auth-token');

    if (!token) {
        return res.status(200).json({
            loggedIn: false,
            msg: 'Información de autenticación inválida'
        });
    }

    try {
        // extraer uid del payload
        const { uid: authUid } = jwt.verify(token, process.env.SECRET_JWT_KEY);

        // Obtener el usuario que realiza la petición        
        const { rol: rolAuth, modelo: usuarioAuth } = await getUsuarioById(authUid);
        // Prevenir que ingrese si el usuario no existe
        if (!usuarioAuth) {
            return res.status(200).json({
                loggedIn: false,
                msg: 'Información de autenticación inválida1'
            })
        }

        // Verificar si el estado del usuario es activo
        if (!usuarioAuth.estado) {
            return res.status(200).json({
                loggedIn: false,
                msg: 'Información de autenticación inválida2'
            });
        }

        // añadir el usuario a la petición
        /* req.usuarioAuth = usuarioAuth.toJSON();
        req.rolAuth = rolAuth;
        next(); */
        res.status(200).json({
            msg: 'Valid JWT',
            loggedIn: true
        })

    } catch (error) {
        console.log(error);
        res.status(200).json({
            loggedIn: false,
            msg: 'Información de autenticación inválida3'
        });
    }
}

module.exports = {
    login,
    isValidJWT
}