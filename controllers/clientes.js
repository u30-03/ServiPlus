const { response } = require('express');
const { Cliente } = require('../models');

const createCliente = async (req, res = response) => {
    try {
        const { nombreCompleto, email, telefono } = req.body;

        const dataCliente = {
            nombreCompleto,
            email: email.toLowerCase(),
            telefono
        }

        const cliente = new Cliente(dataCliente);
        await cliente.save();

        res.status(201).json({
            msg: 'Cliente creado',
            cliente
        });

    } catch (error) {
        console.log(error);
        return res.status(500).json({
            msg: 'Error registrando el nuevo cliente'
        });
    }
}


const getCliente = async (req, res = response) => {
    const { id } = req.params;
    const clienteDB = await Cliente.findById(id);
    res.json({
        cliente: clienteDB
    })
}

const listCliente = async (req, res = response) => {
    const { limit = 5, from = 0 } = req.query;
    const qry = { estado: true };

    const [total, clientes] = await Promise.all([
        Cliente.countDocuments(qry),
        Cliente.find(qry)
            .skip(Number(from))
            .limit(Number(limit))
    ])

    res.json({
        total, clientes
    });
}


const updateCliente = async (req, res = response) => {

    try {
        const { id } = req.params;
        const { _id, ...theCliente } = req.body; //prevenir sobreescritura de _id

        // controlar estados truthy y falsy, estado no es validado
        if (theCliente.estado !== null && theCliente.estado !== undefined) {
            theCliente.estado = theCliente.estado ? true : false;
        }

        if (theCliente.email) {
            theCliente.email = theCliente.email.toLowerCase()
        }

        const clienteDB = await Cliente.findByIdAndUpdate(id, theCliente, { new: true });

        res.json({
            msg: 'cliente actualizado',
            clienteDB
        });

    } catch (error) {
        console.log(error);
        return res.status(500).json({
            msg: 'Error actualizando el cliente'
        });
    }

}

const deleteCliente = async (req, res = response) => {
    const { id } = req.params;
    const clienteDB = await Cliente.findByIdAndUpdate(id, { estado: false }, { new: true });
    res.status(202).json({
        clienteDB
    });
}

module.exports = {
    createCliente,
    getCliente,
    listCliente,
    updateCliente,
    deleteCliente
}