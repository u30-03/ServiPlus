const { response } = require('express');
const { Estado } = require('../models');

const createEstado = async (req, res = response) => {
    try {
        const { nombreEstado } = req.body;

        const dataEstado = {
            nombreEstado: nombreEstado.toUpperCase()
        }

        const estado = new Estado(dataEstado);
        await estado.save();

        res.status(201).json({
            msg: 'Estado creado',
            estado
        });

    } catch (error) {
        console.log(error);
        return res.status(500).json({
            msg: 'Error registrando el nuevo estado'
        });
    }
}


const getEstado = async (req, res = response) => {
    const { id } = req.params;
    const estadoDB = await Estado.findById(id);
    res.json({
        estado: estadoDB
    })
}

const listEstado = async (req, res = response) => {
    const { limit = 5, from = 0 } = req.query;
    const qry = { estado: true };

    const [total, estados] = await Promise.all([
        Estado.countDocuments(qry),
        Estado.find(qry)
            .skip(Number(from))
            .limit(Number(limit))
    ])

    res.json({
        total, estados
    });
}


const updateEstado = async (req, res = response) => {

    try {
        const { id } = req.params;
        const { _id, ...theEstado } = req.body; //prevenir sobreescritura de _id

        //si el nombre del estado existe pero es ella misma, se deja actualizar
        theEstado.nombreEstado = theEstado.nombreEstado.toUpperCase();
        const estadoCheck = await Estado.findOne({ nombreEstado: theEstado.nombreEstado });
        if (estadoCheck && estadoCheck.id !== id) {
            return res.status(400).json({
                msg: 'Ya existe otro estado con el mismo nombre'
            });
        }

        // controlar estados truthy y falsy, estado no es validado
        if (theEstado.estado !== null && theEstado.state !== undefined) {
            theEstado.estado = theEstado.estado ? true : false;
        }

        const estadoDB = await Estado.findByIdAndUpdate(id, theEstado, { new: true });

        res.json({
            msg: 'Estado actualizado',
            estadoDB
        });

    } catch (error) {
        console.log(error);
        return res.status(500).json({
            msg: 'Error actualizando el estado'
        });
    }

}

const deleteEstado = async (req, res = response) => {
    const { id } = req.params;
    const estadoDB = await Estado.findByIdAndUpdate(id, { estado: false }, { new: true });
    res.status(202).json({
        estadoDB
    });
}

module.exports = {
    createEstado,
    getEstado,
    listEstado,
    updateEstado,
    deleteEstado
}