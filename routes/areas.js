const { Router } = require('express');
const { check, query } = require('express-validator');
const { createArea, listArea, getArea, updateArea, deleteArea } = require('../controllers/areas');
const { areaExistsById, areaAlreadyExistsByNombre } = require('../helpers/validaciones-db');
const { validarCampos, validarJWT, validarRol } = require('../middlewares');

const router = Router();

// crear nuevo area
router.post('/', [
    validarJWT,
    validarRol('admin'),
    check('nombreArea', `El nombre es obligatorio`).not().isEmpty(),
    //check('nombreArea').custom(areaAlreadyExistsByNombre),
    validarCampos
], createArea);

// consultar un area
router.get('/:id', [
    validarJWT,
    validarRol('admin', 'agente'),
    check('id', 'El id proporcionado no es válido').isMongoId(),
    check('id').custom(areaExistsById),
    validarCampos
], getArea);

// listar todos los area, paginado
router.get('/', [
    validarJWT,
    validarRol('admin', 'agente'),
    query('limit').optional().isNumeric().withMessage(`El parámetro 'limit' debe ser numérico`),
    query('from').optional().isNumeric().withMessage(`El parámetro 'from' debe ser numérico`),
    validarCampos
], listArea);


// actualizar un area
router.put('/:id', [
    validarJWT,
    validarRol('admin'),
    check('id', 'El id proporcionado no es válido').isMongoId(),
    check('id').custom(areaExistsById),
    check('nombreArea', `El nombre es obligatorio`).not().isEmpty(),
    validarCampos
], updateArea);


// borrar (estado = falso) un area
router.delete('/:id', [
    validarJWT,
    validarRol('admin'),
    check('id', 'El id proporcionado no es válido').isMongoId(),
    check('id').custom(areaExistsById),
    validarCampos
], deleteArea);


module.exports = router;