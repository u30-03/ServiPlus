const { Router } = require('express');
const { check, query } = require('express-validator');
const { createAdmin, listAdmin, getAdmin, updateAdmin, deleteAdmin } = require('../controllers/admins');
const { adminExistsById, emailExistsAdmin } = require('../helpers/validaciones-db');
const { validarPassword } = require('../helpers/validar-pwd');
const { validarCampos, validarJWT, validarRol, validarUpdateEmailAdmin } = require('../middlewares');

const router = Router();

// crear nuevo admin
router.post('/', [
    validarJWT,
    validarRol('admin'),
    check('nombreCompleto', `El nombre es obligatorio`).not().isEmpty(),
    check('email', `El email no es válido`).isEmail(),
    /* check('email').custom(emailExistsAdmin), */
    check('telefono', `El teléfono es obligatorio`).not().isEmpty(),
    check('password').custom((password) => validarPassword(password, false)),
    validarCampos
], createAdmin);

// consultar un admin
router.get('/:id', [
    validarJWT,
    check('id', 'El id proporcionado no es válido').isMongoId(),
    check('id').custom(adminExistsById),
    validarCampos
], getAdmin);

// listar todos los admin, paginado
router.get('/', [
    validarJWT,
    validarRol('admin'),
    query('limit').optional().isNumeric().withMessage(`El parámetro 'limit' debe ser numérico`),
    query('from').optional().isNumeric().withMessage(`El parámetro 'from' debe ser numérico`),
    validarCampos
], listAdmin);


// actualizar un admin
router.put('/:id', [
    validarJWT,
    validarRol('admin'),
    check('id', 'El id proporcionado no es válido').isMongoId(),
    check('id').custom(adminExistsById),
    check('nombreCompleto', `El nombre no puede estar en blanco`).optional().not().isEmpty(),
    check('email', `El email no es válido`).optional().isEmail(),
    validarUpdateEmailAdmin,
    check('telefono', `El teléfono no puede estar en blanco`).optional().not().isEmpty(),
    check('password', 'El password no puede estar en blanco').optional().not().isEmpty(),
    check('password').custom((password) => validarPassword(password, true)),
    validarCampos
], updateAdmin);


// borrar (estado = falso) un admin
router.delete('/:id', [
    validarJWT,
    validarRol('admin'),
    check('id', 'El id proporcionado no es válido').isMongoId(),
    check('id').custom(adminExistsById),
    validarCampos
], deleteAdmin);


module.exports = router;