const { Router } = require('express');
const { check } = require('express-validator');
const { login, isValidJWT } = require('../controllers/auth');
const { validarCampos, agregarAdmin, agregarAgente } = require('../middlewares');
/* import { check, query } from 'express-validator';
*/

const router = Router();

router.post('/admin', [
    agregarAdmin,
    check('email', 'El email no es válido').isEmail(),
    check('password', `La contraseña no puede estar en blanco`).not().isEmpty(),
    validarCampos
], login);

router.post('/agente', [
    agregarAgente,
    check('email', 'El email no es válido').isEmail(),
    check('password', `La contraseña no puede estar en blanco`).not().isEmpty(),
    validarCampos
], login);

router.get('/check', [
    isValidJWT
], () => { })

/* router.post('/login', [
    check('email', 'Not a valid email').isEmail(),
    check('password', `Password can't be empty`).not().isEmpty(),
    validateFields
], login);

router.post('/google-auth', [
    check('id_token', 'ID Token required').not().isEmpty(),
    validateFields
], googleSignIn) */

module.exports = router;