const { Router } = require('express');
const { check, query } = require('express-validator');
const { createAgente, getAgente, listAgente, deleteAgente, updateAgente } = require('../controllers/agentes');
const { emailExistsAgente, areaExistsByNombre, agenteExistsById } = require('../helpers/validaciones-db');
const { validarPassword } = require('../helpers/validar-pwd');
const { validarCampos, validarJWT, validarRol, validarUpdateEmailAgente } = require('../middlewares');

const router = Router();

// crear nuevo agente
router.post('/', [
    validarJWT,
    validarRol('admin'),
    check('nombreCompleto', `El nombre es obligatorio`).not().isEmpty(),
    check('email', `El email no es válido`).isEmail(),
    /* check('email').custom(emailExistsAgente), */
    check('telefono', `El teléfono es obligatorio`).not().isEmpty(),
    check('password').custom((password) => validarPassword(password, false)),
    check('area', 'El área es obligatoria').not().isEmpty(),
    check('area').custom(areaExistsByNombre),
    validarCampos
], createAgente);

// consultar un agente
router.get('/:id', [
    validarJWT,
    check('id', 'El id proporcionado no es válido').isMongoId(),
    check('id').custom(agenteExistsById),
    validarCampos
], getAgente);


// listar todos los agente, paginado
router.get('/', [
    validarJWT,
    query('limit').optional().isNumeric().withMessage(`El parámetro 'limit' debe ser numérico`),
    query('from').optional().isNumeric().withMessage(`El parámetro 'from' debe ser numérico`),
    validarCampos
], listAgente);

// borrar (estado = falso) un agente
router.delete('/:id', [
    validarJWT,
    validarRol('admin'),
    check('id', 'El id proporcionado no es válido').isMongoId(),
    check('id').custom(agenteExistsById),
    validarCampos
], deleteAgente);


// actualizar un agente
router.put('/:id', [
    validarJWT,
    validarRol('admin'),
    check('id', 'El id proporcionado no es válido').isMongoId(),
    check('id').custom(agenteExistsById),
    check('nombreCompleto', `El nombre no puede estar en blanco`).optional().not().isEmpty(),
    check('email', `El email no es válido`).optional().isEmail(),
    validarUpdateEmailAgente,
    check('telefono', `El teléfono no puede estar en blanco`).optional().not().isEmpty(),
    check('password', 'El password no puede estar en blanco').optional().not().isEmpty(),
    check('password').custom((password) => validarPassword(password, true)),
    check('area', 'El área no puede estar en blanco').optional().not().isEmpty(),
    check('area').optional().custom(areaExistsByNombre),
    validarCampos
], updateAgente);


module.exports = router;


