
const { Router } = require('express');
const { check, query } = require('express-validator');
const { createHistoria, getHistoria, listHistoria, deleteHistoria, updateHistoria } = require('../controllers/historias');
const { historiaExistsById, emailExistsCliente, tipoExistsByNombre, estadoExistsByNombre, ticketExistsById, emailExistsAgente } = require('../helpers/validaciones-db');
const { validarCampos, validarJWT, validarRol } = require('../middlewares');

const router = Router();


// crear nuevo historia
router.post('/', [
    validarJWT,
    check('idTicket', `El ticket es obligatorio`).not().isEmpty(),
    check('idTicket', `El ticket no es válido`).isMongoId(),
    check('idTicket', `El ticket no existe`).custom(ticketExistsById),
    check('agenteAsigna', 'El agente no existe').optional().custom(emailExistsAgente),
    check('evento', 'El evento no es corecto').isIn(['apertura', 'cierre', 'reasignacion', 'seguimiento']),
    check('observacion', 'La observación debe ser un texto').optional().isString(),
    validarCampos
], createHistoria);


// consultar un historia
router.get('/:id', [
    validarJWT,
    check('id', 'El id proporcionado no es válido').isMongoId(),
    check('id').custom(historiaExistsById),
    validarCampos
], getHistoria);

// listar todos los historia, paginado
router.get('/', [
    validarJWT,
    query('limit').optional().isNumeric().withMessage(`El parámetro 'limit' debe ser numérico`),
    query('from').optional().isNumeric().withMessage(`El parámetro 'from' debe ser numérico`),
    validarCampos
], listHistoria);



// borrar (estado = falso) un historia
router.delete('/:id', [
    validarJWT,
    validarRol('admin'),
    check('id', 'El id proporcionado no es válido').isMongoId(),
    check('id').custom(historiaExistsById),
    validarCampos
], deleteHistoria);


// actualizar un historia
// en realidad una historia no debería actualizarse, lo que se actualiza es el ticket que crea una historia, que es como un log
/* router.put('/:id', [
    validarJWT,
    check('id', 'El id proporcionado no es válido').isMongoId(),
    check('id').custom(historiaExistsById),
    check('agenteAsigna', 'El agente no existe').optional().custom(emailExistsAgente),
    check('evento', 'El evento no debe dejarse en blanco').isString().isLength({min: 2}),
    check('observacion', 'La observación debe ser un texto').optional().isString(),
    validarCampos
], updateHistoria);
 */

module.exports = router;


