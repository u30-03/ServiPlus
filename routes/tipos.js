const { Router } = require('express');
const { check, query } = require('express-validator');
const { createTipo, listTipo, getTipo, updateTipo, deleteTipo } = require('../controllers/tipos');
const { tipoAlreadyExistsByNombre, tipoExistsById } = require('../helpers/validaciones-db');
const { validarCampos, validarJWT, validarRol } = require('../middlewares');

const router = Router();

// crear nuevo tipo
router.post('/', [
    validarJWT,
    validarRol('admin'),
    check('descripcionTipo', `La descripción es obligatoria`).not().isEmpty(),
    check('descripcionTipo').custom(tipoAlreadyExistsByNombre),
    check('tiempo', 'El tiempo debe ser un número entero mayor o igual 0').isInt({ min: 0 }),
    validarCampos
], createTipo);

// consultar un tipo
router.get('/:id', [
    validarJWT,
    validarRol('admin', 'agente'),
    check('id', 'El id proporcionado no es válido').isMongoId(),
    check('id').custom(tipoExistsById),
    validarCampos
], getTipo);

// listar todos los tipos, paginado
router.get('/', [
    validarJWT,
    validarRol('admin', 'agente'),
    query('limit').optional().isNumeric().withMessage(`El parámetro 'limit' debe ser numérico`),
    query('from').optional().isNumeric().withMessage(`El parámetro 'from' debe ser numérico`),
    validarCampos
], listTipo);


// actualizar un area
router.put('/:id', [
    validarJWT,
    validarRol('admin'),
    check('id', 'El id proporcionado no es válido').isMongoId(),
    check('id').custom(tipoExistsById),
    check('descripcionTipo', `La descripción no puede estar en blanco`).optional().not().isEmpty(),
    check('tiempo', 'El tiempo debe ser un número entero mayor o igual 0').optional().isInt({ min: 0 }),
    validarCampos
], updateTipo);


// borrar (estado = falso) un area
router.delete('/:id', [
    validarJWT,
    validarRol('admin'),
    check('id', 'El id proporcionado no es válido').isMongoId(),
    check('id').custom(tipoExistsById),
    validarCampos
], deleteTipo);


module.exports = router;