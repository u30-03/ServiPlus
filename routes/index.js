const authRouter = require('./auth');
const adminRouter = require('./admins');
const agenteRouter = require('./agentes');
const areaRouter = require('./areas');
const tipoRouter = require('./tipos');
const estadoRouter = require('./estados');
const clienteRouter = require('./clientes');
const ticketRouter = require('./tickets');
const historiaRouter = require('./historias');

module.exports = {
    authRouter,
    adminRouter,
    agenteRouter,
    areaRouter,
    tipoRouter,
    estadoRouter,
    clienteRouter,
    ticketRouter,
    historiaRouter
}