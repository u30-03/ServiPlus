const { Router } = require('express');
const { check, query } = require('express-validator');
const { createCliente, listCliente, getCliente, updateCliente, deleteCliente } = require('../controllers/clientes');
const { clienteExistsById, clienteAlreadyExistsByEmail } = require('../helpers/validaciones-db');
const { validarCampos, validarJWT, validarUpdateEmailCliente } = require('../middlewares');

const router = Router();

// crear nuevo cliente
router.post('/', [
    check('nombreCompleto', `El nombre es obligatorio`).not().isEmpty(),
    check('email', `El email no es válido`).isEmail(),
    check('email').custom(clienteAlreadyExistsByEmail),
    check('telefono', `El teléfono es obligatorio`).not().isEmpty(),
    validarCampos
], createCliente);

// consultar un cliente
router.get('/:id', [
    validarJWT,
    check('id', 'El id proporcionado no es válido').isMongoId(),
    check('id').custom(clienteExistsById),
    validarCampos
], getCliente);

// listar todos los cliente, paginado
router.get('/', [
    validarJWT,
    query('limit').optional().isNumeric().withMessage(`El parámetro 'limit' debe ser numérico`),
    query('from').optional().isNumeric().withMessage(`El parámetro 'from' debe ser numérico`),
    validarCampos
], listCliente);


// actualizar un cliente
router.put('/:id', [
    validarJWT,
    check('id', 'El id proporcionado no es válido').isMongoId(),
    check('id').custom(clienteExistsById),
    check('nombreCompleto', `El nombre no puede estar en blanco`).optional().not().isEmpty(),
    check('email', `El email no es válido`).optional().isEmail(),
    validarUpdateEmailCliente,
    check('telefono', `El teléfono no puede estar en blanco`).optional().not().isEmpty(),
    validarCampos
], updateCliente);


// borrar (estado = falso) un cliente
router.delete('/:id', [
    validarJWT,
    check('id', 'El id proporcionado no es válido').isMongoId(),
    check('id').custom(clienteExistsById),
    validarCampos
], deleteCliente);
/*  */

module.exports = router;