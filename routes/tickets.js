
const { Router } = require('express');
const { check, query } = require('express-validator');
const { createTicket, getTicket, listTicket, deleteTicket, updateTicket, getUltHistoriaTicket } = require('../controllers/tickets');
const { ticketExistsById, emailExistsCliente, tipoExistsByNombre, estadoExistsByNombre } = require('../helpers/validaciones-db');
const { validarCampos, validarJWT, validarRol } = require('../middlewares');

const router = Router();


// crear nuevo ticket
router.post('/', [
    check('cliente', `El cliente es obligatorio`).not().isEmpty(),
    check('cliente', `El email del cliente no es válido`).isEmail(),
    check('cliente').custom(emailExistsCliente),
    check('tipoTicket', 'El tipo de ticket es obligatorio').not().isEmpty(),
    check('tipoTicket', 'El tipo de ticket ingresado no existe').custom(tipoExistsByNombre),
    check('estadoProceso', 'El estado es obligatorio').not().isEmpty(),
    check('estadoProceso', 'El estado ingresado no existe').custom(estadoExistsByNombre),
    check('descripcion', 'La descripción es obligatoria').isLength({ min: 10 }),
    /* check('fechaApertura', 'La fecha de apertura no es válida').optional().isDate(), */
    check('fechaApertura', 'La fecha de apertura no es válida').optional().isISO8601().toDate(),
    validarCampos
], createTicket);


// consultar un ticket
router.get('/:id', [
    validarJWT,
    check('id', 'El id proporcionado no es válido').isMongoId(),
    check('id').custom(ticketExistsById),
    validarCampos
], getTicket);


// listar todos los ticket, paginado
router.get('/', [
    validarJWT,
    query('limit').optional().isNumeric().withMessage(`El parámetro 'limit' debe ser numérico`),
    query('from').optional().isNumeric().withMessage(`El parámetro 'from' debe ser numérico`),
    validarCampos
], listTicket);

router.get('/:id/last', [
    validarJWT,
    check('id', 'El id proporcionado no es válido').isMongoId(),
    check('id').custom(ticketExistsById),
    validarCampos
], getUltHistoriaTicket);


// borrar (estado = falso) un ticket
router.delete('/:id', [
    validarJWT,
    check('id', 'El id proporcionado no es válido').isMongoId(),
    check('id').custom(ticketExistsById),
    validarCampos
], deleteTicket);


// actualizar un ticket
router.put('/:id', [
    validarJWT,
    check('id', 'El id proporcionado no es válido').isMongoId(),
    check('id').custom(ticketExistsById),
    check('cliente', `El cliente es obligatorio`).optional().not().isEmpty(),
    check('cliente', `El email del cliente no es válido`).optional().isEmail(),
    check('cliente').optional().custom(emailExistsCliente),
    check('estadoProceso', 'El estado es obligatorio').optional().not().isEmpty(),
    check('estadoProceso', 'El estado ingresado no existe').optional().custom(estadoExistsByNombre),
    check('descripcion', 'La descripción es obligatoria').optional().isLength({ min: 10 }),
    /* check('solucion', 'La solución es obligatoria').optional().isLength({ min: 10 }), */
    /* check('fechaApertura', 'La fecha de apertura no es válida').optional().isDate(),
    check('fechaCierre', 'La fecha de cierre no es válida').optional().isDate(), */
    check('fechaApertura', 'La fecha de apertura no es válida').optional().isISO8601().toDate(),
    check('fechaCierre', 'La fecha de cierre no es válida').optional().isISO8601().toDate(),
    validarCampos
], updateTicket);


module.exports = router;


