const { Router } = require('express');
const { check, query } = require('express-validator');
const { createEstado, listEstado, getEstado, updateEstado, deleteEstado } = require('../controllers/estados');
const { estadoExistsById, estadoAlreadyExistsByNombre } = require('../helpers/validaciones-db');
const { validarCampos, validarJWT, validarRol } = require('../middlewares');

const router = Router();

// crear nuevo estado
router.post('/', [
    validarJWT,
    validarRol('admin'),
    check('nombreEstado', `El nombre es obligatorio`).not().isEmpty(),
    check('nombreEstado').custom(estadoAlreadyExistsByNombre),
    validarCampos
], createEstado);

// consultar un estado
router.get('/:id', [
    validarJWT,
    validarRol('admin', 'agente'),
    check('id', 'El id proporcionado no es válido').isMongoId(),
    check('id').custom(estadoExistsById),
    validarCampos
], getEstado);

// listar todos los estado, paginado
router.get('/', [
    validarJWT,
    validarRol('admin', 'agente'),
    query('limit').optional().isNumeric().withMessage(`El parámetro 'limit' debe ser numérico`),
    query('from').optional().isNumeric().withMessage(`El parámetro 'from' debe ser numérico`),
    validarCampos
], listEstado);


// actualizar un estado
router.put('/:id', [
    validarJWT,
    validarRol('admin'),
    check('id', 'El id proporcionado no es válido').isMongoId(),
    check('id').custom(estadoExistsById),
    check('nombreEstado', `El nombre es obligatorio`).not().isEmpty(),
    validarCampos
], updateEstado);


// borrar (estado = falso) un estado
router.delete('/:id', [
    validarJWT,
    validarRol('admin'),
    check('id', 'El id proporcionado no es válido').isMongoId(),
    check('id').custom(estadoExistsById),
    validarCampos
], deleteEstado);


module.exports = router;