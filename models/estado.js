const { Schema, model } = require('mongoose');

const EstadoSchema = Schema({
    nombreEstado: {
        type: String,
        required: [true, 'El nombre del estado es requerido']
    },
    estado: {
        type: Boolean,
        required: [true, 'El estado es requerido'],
        default: true
    }
});

EstadoSchema.methods.toJSON = function () {
    const { __v, ...restoEstado } = this.toObject();
    return restoEstado;
}


module.exports = model('Estado', EstadoSchema, 'estados');
