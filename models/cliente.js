const { Schema, model } = require('mongoose');

const ClienteSchema = Schema({
    nombreCompleto: {
        type: String,
        required: [true, 'El nombre es requerido']
    },
    email: {
        type: String,
        required: [true, 'El email es requerido'],
        unique: true
    },
    telefono: {
        type: String,
        required: [true, 'El teléfono es requerido']
    },
    estado: {
        type: Boolean,
        default: true
    }
});

ClienteSchema.methods.toJSON = function () {
    const { __v, ...restoCliente } = this.toObject();
    return restoCliente;
}


module.exports = model('Cliente', ClienteSchema, 'clientes');
