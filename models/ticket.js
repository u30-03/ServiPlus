const { Schema, model } = require('mongoose');

const TicketSchema = Schema({
    cliente: {
        type: Schema.Types.ObjectId,
        ref: 'Cliente',
        required: [true, 'El cliente es requerido']
    },
    tipoTicket: {
        type: Schema.Types.ObjectId,
        ref: 'Tipo',
        required: [true, 'El tipo de ticket es requerido']
    },/* 
    ultHistoria: {
        type: Schema.Types.ObjectId,
        ref: 'Historia'
    }, */
    estadoProceso: {
        type: Schema.Types.ObjectId,
        ref: 'Estado',
        required: [true, 'El estado es requerido']
    },
    fechaApertura: {
        type: Date,
        required: [true, 'Se requiere una fecha de apertura']
    },
    fechaCierre: {
        type: Date
    },
    descripcion: {
        type: String,
        required: [true, 'Se requiere una descripción del caso']
    },
    solucion: {
        type: String
    },
    estado: {
        type: Boolean,
        required: [true, 'El estado es requerido'],
        default: true
    }
});

TicketSchema.methods.toJSON = function () {
    const { __v, ...restoTicket } = this.toObject();
    return restoTicket;
}


module.exports = model('Ticket', TicketSchema, 'tickets');
