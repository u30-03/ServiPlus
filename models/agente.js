const { Schema, model } = require('mongoose');

const AgenteSchema = Schema({
    nombreCompleto: {
        type: String,
        required: [true, 'El nombre es requerido']
    },
    email: {
        type: String,
        required: [true, 'El email es requerido'],
        unique: true
    },
    telefono: {
        type: String,
        required: [true, 'El teléfono es requerido']
    },
    password: {
        type: String,
        required: [true, 'El password es requerido']
    },
    solicitarCambio: {
        type: Boolean,
        default: false
    },
    area: {
        type: Schema.Types.ObjectId,
        ref: 'Area',
        required: [true, 'El área es requerida']
    },
    estado: {
        type: Boolean,
        required: [true, 'El estado es requerido'],
        default: true
    }
});

AgenteSchema.methods.toJSON = function () {
    const { __v, password, _id, ...restoAgente } = this.toObject();
    restoAgente.uid = _id;
    return restoAgente;
}

module.exports = model('Agente', AgenteSchema, 'agentes');
