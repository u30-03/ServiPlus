const { Schema, model } = require('mongoose');

const AreaSchema = Schema({
    nombreArea: {
        type: String,
        required: [true, 'El nombre del área es requerido']
    },
    estado: {
        type: Boolean,
        default: true
    }
});

AreaSchema.methods.toJSON = function () {
    const { __v, ...restoArea } = this.toObject();
    return restoArea;
}


module.exports = model('Area', AreaSchema, 'areas');