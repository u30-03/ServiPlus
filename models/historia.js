const { Schema, model } = require('mongoose');

const HistoriaSchema = Schema({
    ticket: {
        type: Schema.Types.ObjectId,
        ref: 'Ticket',
        required: [true, 'El ticket es requerido']
    },
    tipoTicket: {
        type: Schema.Types.ObjectId,
        ref: 'Tipo',
        required: [true, 'El tipo de ticket es requerido']
    },
    estadoProceso: {
        type: Schema.Types.ObjectId,
        ref: 'Estado',
        required: [true, 'El estado es requerido']
    },
    observacion: {
        type: String
    },
    agenteAsigna: {
        type: Schema.Types.ObjectId,
        ref: 'Agente'
    },
    areaAsigna: {
        type: Schema.Types.ObjectId,
        ref: 'Area'
    },
    evento: {
        type: String,
        required: [true, 'Se requiere un evento'],
        enum: ['apertura', 'cierre', 'reasignacion', 'seguimiento']
    },
    fechaMod: {
        type: Date,
        required: [true, 'La fecha de modificación es requerida']
    },
    agenteMod: {
        type: Schema.Types.ObjectId,
        ref: 'Agente'
    },
    adminMod: {
        type: Schema.Types.ObjectId,
        ref: 'Admin'
    }
});


HistoriaSchema.methods.toJSON = function () {
    const { __v, _id, ...restoHistoria } = this.toObject();
    return restoHistoria;
}



module.exports = model('Historia', HistoriaSchema, 'historias');
