const Admin = require('./admin');
const Agente = require('./agente');
const Area = require('./area');
const Cliente = require('./cliente');
const Estado = require('./estado');
const Historia = require('./historia');
const Ticket = require('./ticket');
const Tipo = require('./tipo');

module.exports = {
    Admin,
    Agente,
    Area,
    Cliente,
    Estado,
    Historia,
    Ticket,
    Tipo
}