const { Schema, model } = require('mongoose');

const TipoSchema = Schema({
    descripcionTipo: {
        type: String,
        required: [true, 'La descripción del tipo de ticket es requerida']
    },
    tiempo: {
        type: Number,
        required: [true, 'Se requiere un tiempo de ANS para el tipo de ticket (número de días)'],
        validate: {
            validator: Number.isInteger,
            message: `El número {VALUE} no es un entero válido`
        }
    },
    estado: {
        type: Boolean,
        required: [true, 'El estado es requerido'],
        default: true
    }
});

TipoSchema.methods.toJSON = function () {
    const { __v, ...restoTipo } = this.toObject();
    return restoTipo;
}

module.exports = model('Tipo', TipoSchema, 'tipos');
