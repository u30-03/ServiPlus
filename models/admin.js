const { Schema, model } = require('mongoose');

const AdminSchema = Schema({
    nombreCompleto: {
        type: String,
        required: [true, 'El nombre es requerido']
    },
    email: {
        type: String,
        required: [true, 'El email es requerido'],
        unique: true
    },
    telefono: {
        type: String,
        required: [true, 'El teléfono es requerido']
    },
    password: {
        type: String,
        required: [true, 'El password es requerido']
    },
    solicitarCambio: {
        type: Boolean,
        default: false
    },
    estado: {
        type: Boolean,
        default: true
    }
});

AdminSchema.methods.toJSON = function () {
    const { __v, password, _id, ...restoAdmin } = this.toObject();
    restoAdmin.uid = _id;
    return restoAdmin;
}

module.exports = model('Admin', AdminSchema, 'administradores');