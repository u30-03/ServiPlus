const express = require('express');
const cors = require('cors');
const { dbConnection } = require('../database/config.db');
const { authRouter, adminRouter, areaRouter, agenteRouter, estadoRouter, tipoRouter, clienteRouter, ticketRouter, historiaRouter } = require('../routes');

class Server {
    constructor() {
        // config server
        this.app = express();
        this.port = process.env.PORT;

        this.paths = {
            auth: '/api/auth',
            admins: '/api/admins',
            agentes: '/api/agentes',
            areas: '/api/areas',
            estados: '/api/estados',
            tipos: '/api/tipos',
            clientes: '/api/clientes',
            tickets: '/api/tickets',
            historias: '/api/historias'
        }

        //conectar a la base de datos
        this.connectDB();

        // middlewares
        this.middlewares();

        // rutas
        this.routes();
    }

    async connectDB() {
        await dbConnection();
    }

    middlewares() {
        // cors
        this.app.use(cors());

        // Parsear y leer body
        this.app.use(express.json());

        // carpeta pública
        this.app.use(express.static('public'));

    }

    routes() {
        this.app.use(this.paths.auth, authRouter);
        this.app.use(this.paths.admins, adminRouter);
        this.app.use(this.paths.agentes, agenteRouter);
        this.app.use(this.paths.areas, areaRouter);
        this.app.use(this.paths.tipos, tipoRouter);
        this.app.use(this.paths.estados, estadoRouter);
        this.app.use(this.paths.clientes, clienteRouter);
        this.app.use(this.paths.tickets, ticketRouter);
        this.app.use(this.paths.historias, historiaRouter)
        /* this.app.use(this.paths.users, require('../routes/users'));*/
    }

    startServer() {
        this.app.listen(this.port, () => {
            console.log(`Servidor corriendo en el puerto ${this.port}...`)
        });
    }

}

module.exports = Server;