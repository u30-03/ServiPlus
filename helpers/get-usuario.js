const { Agente, Admin } = require("../models");

const getUsuarioByEmail = async (coleccion, email) => {
    let modelo;

    switch (coleccion) {
        case 'agente':
            modelo = await Agente.findOne({ email });
            if (!modelo) {
                return undefined;
            }
            return modelo;
            break;

        case 'admin':
            modelo = await Admin.findOne({ email });
            if (!modelo) {
                return undefined;
            }
            return modelo;
            break;

        default:
            return undefined;
            break;
    }
}

const getUsuarioByIdColeccion = async (coleccion, id) => {
    let modelo;

    switch (coleccion) {
        case 'agente':
            modelo = await Agente.findById(id);
            if (!modelo) {
                return undefined;
            }
            return modelo;
            break;

        case 'admin':
            modelo = await Admin.findById(id);
            if (!modelo) {
                return undefined;
            }
            return modelo;
            break;

        default:
            return undefined;
            break;
    }
}

const getUsuarioById = async (id) => {
    let modelo;
    let rol;
    modelo = await Admin.findById(id);
    if (!modelo) {
        modelo = await Agente.findById(id);
        if (!modelo) {
            //return undefined;
        } else {
            rol = 'agente';
        }
    } else {
        rol = 'admin';
    }
    return { rol, modelo };
}

module.exports = {
    getUsuarioByEmail,
    getUsuarioById,
    getUsuarioByIdColeccion
}