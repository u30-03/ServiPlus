const { Historia } = require("../models");
const { ObjectId } = require('mongoose').Types;

const getUltimaHistoria = async (idTicket) => {
    try {
        //const uH = await Historia.find({ ticket: idTicket }).sort({ $fechaMod: -1 })    
        const uH = await Historia.find({ ticket: new ObjectId(idTicket) })
            .sort({ fechaMod: -1 })
            .limit(1)
            .populate('estadoProceso', 'nombreEstado')
            .populate('tipoTicket', 'descripcionTipo')
            //.populate('ticket')
            .populate({ path: 'ticket.cliente', populate: 'nombreCompleto' })
            .populate('ticket', ['descripcion', 'cliente'])
            .populate('areaAsigna', 'nombreArea')
            .populate('agenteMod', 'nombreCompleto')
            .populate('adminMod', 'nombreCompleto')
            .populate('agenteAsigna', ['email', 'nombreCompleto'])

        /* 
        .populate('cliente', 'nombreCompleto')
         */
        return uH;
    } catch (error) {
        console.log(error);
        return [];
    }
}

module.exports = {
    getUltimaHistoria
}