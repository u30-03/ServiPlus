const { Admin, Agente, Area, Tipo, Cliente, Estado, Ticket, Historia } = require('../models');

/* Validaciones admins */
const adminExistsById = async (id = '') => {
    const existsDB = await Admin.findById(id);
    if (!existsDB) {
        throw new Error(`El admin no existe`);
    }
}

const emailExistsAdmin = async (email = '') => {
    const existsDB = await Admin.findOne({ email });
    if (existsDB) {
        throw new Error('El correo ya está registrado');
    }
}


/* Validaciones agentes */
const agenteExistsById = async (id = '') => {
    const existsDB = await Agente.findById(id);
    if (!existsDB) {
        throw new Error(`El agente no existe`);
    }
}

const emailExistsAgente = async (email = '') => {
    const existsDB = await Agente.findOne({ email });
    if (existsDB) {
        throw new Error('El correo ya está registrado');
    }
}

const areaExistsByNombre = async (nombreArea = '') => {
    const existsDB = await Area.findOne({ nombreArea: nombreArea.toUpperCase() });
    if (!existsDB) {
        throw new Error('El área no existe');
    }
}

/* Validaciones áreas */
const areaAlreadyExistsByNombre = async (nombreArea = '') => {
    const existsDB = await Area.findOne({ nombreArea: nombreArea.toUpperCase() });
    if (existsDB) {
        throw new Error('El área ya existe');
    }
}

const areaExistsById = async (id = '') => {
    const existsDB = await Area.findById(id);
    if (!existsDB) {
        throw new Error(`El área no existe`);
    }
}

/* Validaciones estados */
const estadoAlreadyExistsByNombre = async (nombreEstado = '') => {
    const existsDB = await Estado.findOne({ nombreEstado: nombreEstado.toUpperCase() });
    if (existsDB) {
        throw new Error('El estado ya existe');
    }
}

const estadoExistsById = async (id = '') => {
    const existsDB = await Estado.findById(id);
    if (!existsDB) {
        throw new Error(`El estado no existe`);
    }
}

const estadoExistsByNombre = async (nombreEstado = '') => {
    const existsDB = await Estado.findOne({ nombreEstado: nombreEstado.toUpperCase() });
    if (!existsDB) {
        throw new Error(`El estado no existe`);
    }
}


/* Validaciones tipos */
const tipoAlreadyExistsByNombre = async (descripcionTipo = '') => {
    const existsDB = await Tipo.findOne({ descripcionTipo: descripcionTipo.toUpperCase() });
    if (existsDB) {
        throw new Error('El tipo ya existe');
    }
}

const tipoExistsById = async (id = '') => {
    const existsDB = await Tipo.findById(id);
    if (!existsDB) {
        throw new Error(`El tipo no existe`);
    }
}

const tipoExistsByNombre = async (descripcionTipo) => {
    const existsDB = await Tipo.findOne({ descripcionTipo: descripcionTipo.toUpperCase() });
    if (!existsDB) {
        throw new Error(`El tipo no existe`);
    }
}




/* Validaciones clientes */
const clienteExistsById = async (id = '') => {
    const existsDB = await Cliente.findById(id);
    if (!existsDB) {
        throw new Error(`El cliente no existe`);
    }
}

const emailExistsCliente = async (email = '') => {
    const existsDB = await Cliente.findOne({ email });
    if (!existsDB) {
        throw new Error(`El cliente no existe`);
    }
}

const clienteAlreadyExistsByEmail = async (email = '') => {
    const existsDB = await Cliente.findOne({ email });
    if (existsDB) {
        throw new Error('El correo del cliente ya está registrado');
    }
}


/* Validaciones tickets */
const ticketExistsById = async (id = '') => {
    const existsDB = await Ticket.findById(id);
    if (!existsDB) {
        throw new Error(`El ticket no existe`);
    }
}

/* Validaciones historias */
const historiaExistsById = async (id = '') => {
    const existsDB = await Historia.findById(id);
    if (!existsDB) {
        throw new Error(`La historia no existe`);
    }
}



module.exports = {
    adminExistsById, emailExistsAdmin, agenteExistsById, emailExistsAgente,
    areaAlreadyExistsByNombre, areaExistsById, areaExistsByNombre,
    tipoAlreadyExistsByNombre, tipoExistsById, tipoExistsByNombre,
    clienteAlreadyExistsByEmail, emailExistsCliente, clienteExistsById,
    estadoAlreadyExistsByNombre, estadoExistsById, estadoExistsByNombre,
    ticketExistsById,
    historiaExistsById
}

