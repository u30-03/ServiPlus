
const validarPassword = (password = '', updatePass = false) => {

    // si está actualizando el usuario, la contraseña se puede dejar en blanco para que quede igual a la anterior
    if (updatePass && password === '') {
        return true;
    }

    if (typeof password !== 'string') {
        throw new Error('La contraseña debe ser un texto');
    }

    if (password.length < 8 || password.length > 100) {
        throw new Error('El largo de la constraseña debe estar entre 8 y 100');
    }

    if (/^[a-zA-Z]*$/.test(password)) {
        throw new Error('La contraseña debe contener al menos un número');
    }

    if (/^[0-9]*$/.test(password)) {
        throw new Error('La contraseña debe contener al menos un caracter alfanumérico');
    }

    if (password.toUpperCase() === password) {
        throw new Error('La contraseña debe contener al menos una letra minúscula');
    }

    if (password.toLowerCase() === password) {
        throw new Error('La contraseña debe contener al menos una letra minúscula');
    }

    return true;
}

module.exports = {
    validarPassword
}